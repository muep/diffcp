extern crate clap;

use std::ffi::OsStr;
use std::os::unix::process::CommandExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use std::process::Stdio;

use clap::Arg;
use clap::App;

fn find_difftool() -> &'static str {
    let tools = ["colordiff", "diff"];

    for tool in tools.iter() {
        let tool_status = Command::new(tool).arg("--version").stdout(Stdio::null()).status();

        match tool_status {
            Ok(status) => {
                if status.code() == Some(0) {
                    return *tool;
                }
            }
            _ => {}
        }
    }

    panic!("no diff tool available");
}

fn normalize_dest(src: &Path, dest: &Path, dest_is_dir: bool) -> PathBuf {
    if dest_is_dir {
        dest.join(src.file_name().unwrap())
    } else {
        dest.to_path_buf()
    }
}

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .about("Look what you are changing before you copy")
        .version(env!("CARGO_PKG_VERSION"))
        .arg(Arg::with_name("interactive")
             .short("i")
             .long("interactive")
             .help("Print a diff and ask to confirm before copying"))
        .arg(Arg::with_name("filename")
             .help("Files to be copied (last is destination)")
             .required(true)
             .min_values(2)
             .multiple(true))
        .get_matches();

    let diff = find_difftool();

    let interactive = matches.is_present("interactive");

    let filenames: Vec<&OsStr> = matches.values_of_os("filename").unwrap().collect();

    let paths: Vec<&Path> = filenames.iter().map(|p| Path::new(p)).collect();

    let (dest, srcs) = paths.split_last().unwrap();
    let dest_is_dir = dest.is_dir();

    if (!dest_is_dir) && srcs.len() > 1 {
        panic!("many sources and non-directory destination");
    }

    for src in srcs {
        let dest = normalize_dest(src, dest, dest_is_dir);

        if dest.is_dir() {
            panic!("{} is a directory", dest.display());
        }

        Command::new(diff)
            .arg("-Nu")
            .arg(dest)
            .arg(src)
            .spawn()
            .expect("Failed to execute diff")
            .wait()
            .expect("Failed to wait for diff to terminate");
    }

    if interactive {
        let mut buf = String::new();
        println!("apply the above changes? (y/n)");

        std::io::stdin().read_line(&mut buf).expect("Failed to read a line");

        if buf.trim() != "y" {
            println!("Exiting without applying any changes");
            return;
        }
    }

    Command::new("cp").arg("-v").args(&filenames).exec();
}
