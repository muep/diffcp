This repository contains diffcp, a simple convenience tool for
combining diff and cp.

When updating scripts or config files, it is sometimes nice to first
run diff to see what will change and only then proceed with cp with
the same files as its parameters. This tool automates that flow, so
instead of this:

```
$ diff -u old new
$ cp new old
```

You can do this
```
$ diffcp new old
```
